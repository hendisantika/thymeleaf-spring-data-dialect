# Thymeleaf Spring Data Dialect
## Things to do list:
1. Clone this repository: `git clone https://gitlab.com/hendisantika/thymeleaf-spring-data-dialect.git`.
2. Go to the folder itself: `cd thymeleaf-spring-data-dialect`.
3. Run the application: `mvn clean spring-boot:run`.
4. Navigate to your favorite browser: http://localhost:8080/index?size=10

## Screen shot

Index Page

![Index Page](img/index.png "Index Page")

Tambah Data Page

![Tambah Data Page](img/tambah.png "Tambah Data Page")

Details Page

![Details Page](img/details.png "Details Page")

Hapus Page

![Hapus Page](img/hapus.png "Hapus Page")